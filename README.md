# Dark+ Color Preset for iTerm 3

Colors copied from [VSCode](https://code.visualstudio.com/) Dark+ theme's terminal colors. 

Credit goes Microsoft.


## Installation

1) [Download `Dark+.itermcolors`](https://gitlab.com/Cervajz/iterm-dark-plus-color-scheme/raw/master/Dark+.itermcolors?inline=false) file
2) Import it to iTerm: `Preferences => Profiles => Colors => Color Presets ... => Import ...`

## Preview

![Preview 01](preview_01.png)
![Preview 02](preview_02.png)
